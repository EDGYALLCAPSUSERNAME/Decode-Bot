import dataset
import sqlalchemy

db = dataset.connect('sqlite:///replied_to.db')

print("Creating replied to table...")
db.create_table('replied_to')

replied_to_columns = {'comment_id': sqlalchemy.String}

print("Adding columns to replied_to...")
for c_name, c_type in replied_to_columns.items():
    db['replied_to'].create_column(c_name, c_type)

print("Done!")
