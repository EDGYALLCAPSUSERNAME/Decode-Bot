import praw
import time
import logging
import dataset
import requests
import binascii
import OAuth2Util

SUBREDDIT_NAME = ''
IDENTIFIER = '+decodebot'

COMMENT_TEMPLATE = """
Here's the comment decoded:\n
--------------------------\n
{}
"""

MORSE_CODE = {'.-': 'a',     '-...': 'b',   '-.-.': 'c',
              '-..': 'd',    '.': 'e',      '..-.': 'f',
              '--.': 'g',    '....': 'h',   '..': 'i',
              '.---': 'j',   '-.-': 'k',    '.-..': 'l',
              '--': 'm',     '-.': 'n',     '---': 'o',
              '.--.': 'p',   '--.-': 'q',   '.-.': 'r',
              '...': 's',    '-': 't',      '..-': 'u',
              '...-': 'v',   '.--': 'w',    '-..-': 'x',
              '-.--': 'y',   '--..': 'z',
              '-----': '0',  '.----': '1',  '..---': '2',
              '...--': '3',  '....-': '4',  '.....': '5',
              '-....': '6',  '--...': '7',  '---..': '8',
              '----.': '9'}


def bin_to_text(binary_list):
    string = ''
    for binary in binary_list:
        try:
            n = int(binary.rstrip(), 2)
        except ValueError as e:
            return 'Sorry, I couldn\'t decode this\n\n{}'.format(e)

        string += binascii.unhexlify('%x' % n).decode('utf-8')

    return string


def hex_to_text(hex_list):
    string = ''
    print(hex_list)
    for hex_s in hex_list:
        try:
            string += bytes.fromhex(hex_s.rstrip()).decode('utf-8')
        except UnicodeDecodeError as e:
            return 'Sorry, I couldn\'t decode this\n\n{}'.format(e)

    return string


def morse_to_text(morse_list):
    string = ''
    print(morse_list)
    for morse in morse_list:
        if morse == '':
            pass
        else:
            try:
                string += MORSE_CODE[morse.rstrip()]
            except KeyError:
                string += '  *{}:  is invalid*  '.format(morse)

    return string


def find_comments(r, rt):
    comments = r.get_comments(SUBREDDIT_NAME)
    for comment in comments:
        if rt.find_one(comment_id=comment.id) is None:
            if '+decodebot' in comment.body:
                print(comment.body)
                # Break the comment into a list
                l = comment.body.split(' ')
                if l[1].lower() == 'binary':
                    string = bin_to_text(l[2:])
                elif l[1].lower() == 'hex':
                    string = hex_to_text(l[2:])
                elif l[1].lower() == 'morse':
                    string = morse_to_text(l[2:])
                else:
                    comment.reply('I don\'t know what I\'m supposed to do!')
                    data = dict(comment_id=str(comment.id))
                    rt.insert(data)
                    break

                print("Replying to comment...")
                comment.reply(COMMENT_TEMPLATE.format(string))

                # Insert into db
                data = dict(comment_id=str(comment.id))
                rt.insert(data)


def main():
    r = praw.Reddit(user_agent='Decoder v1.0 /u/EDGYALLCAPSUSERNAME')
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    db = dataset.connect('sqlite:///replied_to.db')
    rt = db['replied_to']

    while True:
        try:
            o.refresh()
            find_comments(r, rt)
            print('Sleeping...')
            time.sleep(200)
        except requests.exceptions.ConnectionError as e:
            print("ERROR: Reddit is down...")
            logging.debug("ERROR: Reddit went down")
            time.sleep(200)  # sleep because reddit is down
        except Exception as e:
            print("ERR: {}".format(e))
            logging.debug("ERROR: Exception {}".format(e) +
                          " in main exited")
            exit(1)


if __name__ == '__main__':
    main()
